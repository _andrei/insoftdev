'use strict';

var eventsApp = angular.module('eventsApp', ['ngStorage']);

eventsApp.controller('ExampleController', ['$scope', '$http', '$localStorage', '$sessionStorage', function ($scope, $http, $localStorage, $sessionStorage) {
    $scope.autocompletePickUp = {};
    $scope.email = {};
    $scope.phonenumber = {
        pattern: /^\+?\d{10}$/
    }

    $scope.logIn = function () {
        $http({
            method: 'GET',
            url: '/api/userEmail/' + $scope.email.text
        }).then(function successCallback(response) {
            if (angular.isUndefined(response.data[0])) {
                alert("Email-ul nu este in baza de date");
            } else {
                if (response.data[0].password === $scope.password.text) {
                    $sessionStorage.valueEmail = $scope.email.text;
                    $sessionStorage.valueId = response.data[0].id_user;
                    window.location.assign("http://localhost:8000/EventDetails.html");
                } else {
                    alert("Parola Gresita!");
                }
            }
        }, function errorCallback(response) {
            console.log("error");
        });
    }

    $scope.initMyProfile = function () {

        var email = $sessionStorage.valueEmail;
        var user_id = $sessionStorage.valueId;

        $http({
            method: 'GET',
            url: '/api/user/' + user_id
        }).then(function successCallback(response) {

            $scope.name = response.data[0].name;
            $scope.adress = response.data[0].adress;
            $scope.emailP = response.data[0].email;
            $scope.phoneNumber = response.data[0].phone_number;

            console.log(response);
        }, function errorCallback(response) {
            console.log("error");
        });
    }

    $scope.updateDetails = function () {

        var email = $sessionStorage.valueToShare;
        var user_id = $sessionStorage.valueId;

        $http({
            method: 'PUT',
            url: '/api/user/' + user_id,
            data: {
                'adress': $scope.adress,
                'phoneNumber': $scope.phoneNumber,
                'name': $scope.name,
                'email': $scope.emailP
            }
        }).then(function successCallback(response) {
            $sessionStorage.valueToShare = $scope.emailP;
            console.log($sessionStorage.valueToShare);
            email = $sessionStorage.valueToShare;

            $scope.initMyProfile();


        }, function errorCallback(response) {
            console.log("error");
        });
    }


    $scope.createBooking = function () {

        var user_id = $sessionStorage.valueId;
        console.log(autocompletePickUpName);
        console.log(autocompleteDestinationName);
        console.log(user_id);
        console.log(total);
        console.log(duration.text);
        console.log($scope.selectCard);

        $http({
            method: 'POST',
            url: '/api/booking/create',
            data: {
                'id_user': user_id,
                'from_adress': autocompletePickUpName,
                'to_adress': autocompleteDestinationName,
                'distance': total,
                'price': total,
                'duration': 123,
                'payment_method': $scope.selectCard,
                'status': 'in progress'
            }
        }).then(function successCallback(response) {


        }, function errorCallback(response) {
            console.log("error");
        });
    }

}]);


eventsApp.controller('bookingsCtrl', function ($scope, $http, $localStorage, $sessionStorage) {
    $http.get('/api/booking/' + $sessionStorage.valueId).then(function (data) {
        $scope.bookings = data.data;
        console.log($scope.bookings);
    });
});

eventsApp.controller('carTypeCtrl', function ($scope, $http) {
    $scope.carTypes = {

    }
    $scope.updateCarTypeSelect = function () {
        $http({
            method: 'GET',
            url: 'https://api-test.insoftd.com/v1/operator/car_type?q=[{%22key%22:%22CarType.enabled%22,%22value%22:1,%22op%22:%22=%22}]&order=(CarType.rank%20DESC)',
            headers: {
                'Authorization': 'Basic aW50ZXJuc2hpcEBpbnNvZnRkZXYuY29tOmJhY2tvZmZpY2VAfEAyNDg='
            }
        }).then(function (data) {

            $scope.carTypes = data.data.records;

            console.log($scope.carTypes);

        });
    }
});